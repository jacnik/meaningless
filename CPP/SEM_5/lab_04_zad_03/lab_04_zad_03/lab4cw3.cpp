/********************************/
/* Interfejsy w hierarchii klas */
/********************************/

#include <string>
#include <iostream>
#include <sstream>
using namespace std;

/* ...uzupelnic kod programu... */
class IElement
{
public:
	virtual ~IElement() {};
	virtual string oznaczenie() = 0;
	virtual void zapisz() = 0;
	virtual void odczytaj() = 0;
};

class Zbior
{
public:
	Zbior(string nazwa = "", int maxElements = 1) : nazwa(nazwa), 
													maxElements(maxElements), 
													noElements(0)
	{
		this->elementy = new IElement*[maxElements];
	}

	~Zbior()
	{
		this->ZniszczWszystkie();
	}

	void dodaj(IElement* element) 
	{
		if(this->noElements == this->maxElements) {
			IElement** tmp = this->elementy;
			this->maxElements *= 2;
			this->elementy = new IElement*[this->maxElements];
			for(size_t i = 0; i < this->noElements; ++i)
				this->elementy[i] = tmp[i];

			delete[] tmp;
		} 
		this->elementy[this->noElements++] = element;
	};

	IElement* usun(string name)
	{
		for(size_t i = 0; i < this->noElements; ++i )
		{
			IElement* elem = this->elementy[i];
			if (elem->oznaczenie() == name)
			{
				for(size_t j = i; j < this->noElements - 1; ++j)
				{
					this->elementy[j] = this->elementy[j + 1];
				}
				//delete this->elementy[this->noElements--]; //
				this->noElements--;
				return elem;
			}
		}
		return NULL;
	}

	void ZapiszWszystkie()
	{
		for(size_t i = 0; i < this->noElements; ++i )
		{
			this->elementy[i]->zapisz();
		}
	}

	void OdczytajWszystkie()
	{
		for(size_t i = 0; i < this->noElements; ++i )
		{
			this->elementy[i]->odczytaj();
		}
	}

	void ZniszczWszystkie()
	{
		for(size_t i = 0; i < this->noElements; ++i )
		{
			delete this->elementy[i];
		}
		delete[] this->elementy; // memory access error when deleting Podzbior, why?
	}

private:
	IElement** elementy;
	int maxElements;
	int noElements;
	string nazwa;
};

class Podzbior : public Zbior, public IElement
{
public:
	Podzbior(string nazwa = "", int maxElements = 1) 
		: Zbior(nazwa, maxElements)
	{}

	~Podzbior()
	{
		Zbior::ZniszczWszystkie();
	}

	string oznaczenie() override
	{
		return "Podzbior";
	}
	void zapisz() override
	{
	}
	void odczytaj() override
	{
	}
private:
};

class Liczba : public IElement
{
public:
	Liczba(double wartosc) : wartosc(wartosc) {}

	string oznaczenie() override
	{
		return to_string(static_cast<long double>(this->wartosc));
	}

	void zapisz() override
	{
		// nie wiem co to ma kurwa robic, instruckje sa hujowe
	}

	void odczytaj() override
	{
		// ????
	}

private:
	double wartosc;
};

class Napis : public IElement
{
public:
	Napis(string text) : text(text) {}
	
	string oznaczenie() override
	{
		return this->text;
	}
	void zapisz() override
	{
	}
	void odczytaj() override
	{
	}

private:
	string text;
};

int main()
{         
	Zbior A("zbior nadrzedny", 100);

	A.dodaj( new Liczba(3.14) );
	A.dodaj( new Liczba(2.27) );
	A.dodaj( new Napis("Czerwony") );
	A.dodaj( new Napis("Niebieski") );

	Podzbior *X = new Podzbior("zbior podrzedny", 10);
	{
		X->dodaj( new Napis("okrag") );
		X->dodaj( new Liczba(0.01) );
		X->dodaj( new Liczba(0.05) );
		X->dodaj( new Napis("kwadrat") );
	}
	A.dodaj( X );

	A.dodaj( new Liczba(1.44) );
	A.dodaj( new Napis("Zielony") );

	A.ZapiszWszystkie();
	delete A.usun( "2.27" );
	delete A.usun( "Niebieski" );
	A.ZapiszWszystkie();

	A.OdczytajWszystkie();
	A.ZapiszWszystkie();

	A.ZniszczWszystkie();

	system("pause");
	return 0;
}