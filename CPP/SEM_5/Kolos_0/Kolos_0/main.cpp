﻿// Przykladowe kolokwium -> klasy budynek, lokalizacja, Dom, mieszkanie, garaz

#include <iostream>
#include <string>

using namespace std;


// Zgodnie z zadaniem Budynek ma miec swoja lokalizacje jako obiekt osobnej klasy,
// wiec najpierw trzeba stworzyc lokalizacje co by budynek mogl z niej korzystac
class Lokalizacja {
public:
	Lokalizacja(string miasto = "", string ulica = "", int nr_domu = -1) : 
		miasto(miasto), ulica(ulica), nr_domu(nr_domu) {}

	~Lokalizacja() { /*Pusty destruktor */}

private:
	string miasto;
	string ulica;
	int nr_domu;
};

// Teraz trzeba utworzyc klase Budynek, w budynku wykorzysta sie lokalizacje.
// Dom, Mieszkanie, Garaz beda pochodnymi budynku
class Budynek {
public:
	Budynek(Lokalizacja lokalizacja = Lokalizacja(), double powierzchnia = 0) :
	  lokalizacja(lokalizacja), powierzchnia(powierzchnia) {}

	~Budynek() {/* pusty destruktor */}
	
	virtual double oblicz_roczny_koszt() = 0; // ta metoda bedzie nadpisana w klasach Dom, Mieszkanie, Garaz

// lokalizacja i powierzchnia sa oznaczone jako protected zeby mozna bylo sie do nich odwolywac
// w klasie garaz, patrz metoda 'Garaz operator +'
protected:
	Lokalizacja lokalizacja;
	double powierzchnia;	
};

// Budynek juz jest, ma lokalizacje wiec trzeba utworzyc 3 klasy pochodne:
class Dom : public Budynek {
public:
	Dom(Lokalizacja lokalizacja = Lokalizacja(), double powierzchnia = 0,
		double podatek = 0, double oplaty = 0) : 
			Budynek(lokalizacja, powierzchnia), // Inicjalizacja klasy bazowej, czyli Budynek
			podatek(podatek), oplaty(oplaty) {} // Inicjalizacja zmiennych tej klasy

	~Dom() {/* pusty destruktor */}

	// w klasie pochodniej trzeba napisac implementacje metody wirtualnej z klasy bazowej,
	// tutaj jest to 'oblicz_roczny_koszt'
	double oblicz_roczny_koszt() override {
		return (this->podatek + this->oplaty) * 12; // podatek i oplaty sa miesieczne, wg tresci zadania
	}

private:
	double podatek;
	double oplaty;
};

class Mieszkanie : public Budynek {
public:
	Mieszkanie(Lokalizacja lokalizacja = Lokalizacja(), double powierzchnia = 0, double oplaty = 0) :
		Budynek(lokalizacja, powierzchnia), // Inicjalizacja klasy bazowej, czyli Budynek
		oplaty(oplaty) {} // Inicjalizacja zmiennej tej klasy

	~Mieszkanie() {/* pusty destruktor */}

	double oblicz_roczny_koszt() override {
		return this->oplaty * 12; 
	}

	// zadanie 3: dodac funkcje szablonowa
	template<typename T>
	void dodaj_oplate(T oplata) {
		this->oplaty += oplata;
	}

private:
	double oplaty;
};

class Garaz : public Budynek {
public:
	Garaz(Lokalizacja lokalizacja = Lokalizacja(), double powierzchnia = 0, double podatek = 0) :
		Budynek(lokalizacja, powierzchnia), // Inicjalizacja klasy bazowej, czyli Budynek
	    podatek(podatek) {} // Inicjalizacja zmiennej tej klasy

	~Garaz() {/* pusty destruktor */}

	double oblicz_roczny_koszt() override {
		return this->podatek * 12; 
	}

	// Zadanie 2: w Garazu zaimplementowac operator +
	// chodzi o to zeby mozna bylo dodawac 2 garaze: Garaz g1 + Garaz g2
	const Garaz operator + (const Garaz& other ) const {
		Garaz result(this->lokalizacja, this->powierzchnia + other.powierzchnia, this->podatek + other.podatek);
		return result;
	}

private:
	double podatek;
};


int main() {

	// test lokalizacji:
	Lokalizacja lok1; // pusta lokalizacja - miasto = "", ulica = "", nr_domu = -1 
	Lokalizacja lok2("Siem-ce", "Hugo", 33); // lokalizacja - miasto = "Siem-ce", ulica = "Hugo", nr_domu = 33 
	
	// test Budynku
	//Budynek b1; -> nie da sie utworzyc budynku bo jest virtualny
	//Budynek b2(lok2, 10.5); -> tak samo

	// test Dom
	Dom d1(lok2, 123.5, 2, 3.5);
	cout << "Dom d1 roczny koszt = " << d1.oblicz_roczny_koszt() << endl; // (2 + 3.5) * 12 = 66

	// test Mieszkanie
	Mieszkanie m1(lok2, 22.8, 4.5);
	cout << "Mieszkanie m1 roczny koszt = " << m1.oblicz_roczny_koszt() << endl; // 4.5 * 12 = 54

	// sprawdzenie funkcji szablonowej (zad 3) 
	m1.dodaj_oplate(2);
	cout << "Mieszkanie m1 roczny koszt_2 = " << m1.oblicz_roczny_koszt() << endl; // (4.5 + 2) * 12 = 78
	m1.dodaj_oplate(2.2);
	cout << "Mieszkanie m1 roczny koszt_3 = " << m1.oblicz_roczny_koszt() << endl; // (6.5 + 2.2) * 12 = 104.4

	// test Garaz
	Garaz g1(lok2, 10.5, 2.2);
	cout << "Garaz g1 roczny koszt = " << g1.oblicz_roczny_koszt() << endl; // 2.2 * 12 = 26.4

	// dodawanie garazy (zad 2):
	Lokalizacja lok3("Katowice", "Krasinskiego", 123);
	Garaz g2(lok3, 5.5, 3.8);
	Garaz g3 = g1 + g2; // lokalizajca taka jak ma g1, powierzchnia 10.5 + 5.5 = 16, podatek = 2.2 + 3.8 = 6
	cout << "Garaz g3 roczny koszt = " << g3.oblicz_roczny_koszt() << endl; // 6 * 12 = 72



	cin.ignore();
	return 0;
}