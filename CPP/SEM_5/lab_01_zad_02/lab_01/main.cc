﻿#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <sstream>

using namespace std;

int getInteger(string prompt) {
   int value;
   string line;
   while (true) {
      cout << prompt;
      getline(cin, line);
      istringstream stream(line);
      stream >> value >> ws;
      if (!stream.fail() && stream.eof()) break;
      cout << "Zly format. Jeszcze raz." << endl;
      if (prompt == "") prompt = "Podaj integer: ";
   }
   return value;
}

double getReal(string prompt) {
   double value;
   string line;
   while (true) {
      cout << prompt;
      getline(cin, line);
      istringstream stream(line);
      stream >> value >> ws;
      if (!stream.fail() && stream.eof()) break;
      cout << "Zly format. Jeszcze raz." << endl;
      if (prompt == "") prompt = "Podaj number: ";
   }
   return value;
}

template <typename T>
T GetField(T (*func)(string), string varRepr, T minBound, T maxBound)
{
	string min, max;
	stringstream ss;
	ss << minBound;
	ss >> min;
	ss.clear();
	ss << maxBound;
	ss >> max; 
	while (true) {
		T x = func("Podaj " + varRepr + ": ");
		if (x > maxBound) cout << "Za duzy "+ varRepr +" (max " + max + ")" << endl;
		else {
			if (x < minBound) cout << string("Za maly ")+ varRepr +" (min " + min + ")" << endl;
			else return x;
		}
	}
}

class Punkt
{
public:
	Punkt(double x = 0.0, double y = 0.0, double z = 0.0) : 
	  _x(x < -1 || x > 1 ? 0 : x),
	  _y(y < -1 || y > 1 ? 0 : y),
	  _z(z < -1 || z > 1 ? 0 : z)
	{
	}

	void pobierz()
	{
		// pobierz x, y, z:
		this->_x = GetField<double>(&getReal, "x" , -1, 1);
		this->_y = GetField<double>(&getReal, "y" , -1, 1);
		this->_z = GetField<double>(&getReal, "z" , -1, 1);
	}

	void wyswietl(const char* odstep)
	{
		cout << this->_x << odstep << this->_y << odstep << this->_z << endl;
	}

// Private section;
protected:
	double _x;
	double _y;
	double _z;
};


class Kolor
{

public:
	Kolor(int r = 0, int g = 0, int b = 0) : 
		_r(r < 0 || r > 255 ? 0 : r),
		_g(g < 0 || g > 255 ? 0 : g),
		_b(b < 0 || b > 255 ? 0 : b)
	{
	}

	void pobierz()
	{
		// pobierz R:
		this->_r = GetField<int>(&getInteger, "r" , 0, 255);
		this->_g = GetField<int>(&getInteger, "g" , 0, 255);
		this->_b = GetField<int>(&getInteger, "b" , 0, 255);
	}

	void wyswietl(const char* odstep)
	{
		cout << this->_r << odstep << this->_g << odstep << this->_b << endl;
	}

private:
	int _r;
	int _g;
	int _b;
};

// nazwa punkZKolorem, wydaje się hujowa więc zmieniam
class KolorowyPunkt : public Punkt
{

public:
	KolorowyPunkt(double x = 0.0, double y = 0.0, double z = 0.0, int r = 0, int g = 0, int b = 0) 
		: Punkt(x, y, z), _barwa(r, g, b)
	{
	}

	// Property in cpp, cool
	__declspec ( property ( put = SetBarwa, get = GetBarwa ) ) Kolor Barwa;

	void wyswietl(const char* odstep)
	{
		cout << "Kolorowy Punkt:" << endl << "	Punkt: ";
		Punkt::wyswietl(odstep);
		cout << "	Barwa:";
		this->Barwa.wyswietl(odstep);
	}

	void pobierz()
	{
		cout << "--- Podaj punkt:" << endl;
		Punkt::pobierz();
		this->Barwa.pobierz(); 
	}

	Kolor GetBarwa() const
	{
		return this->_barwa;
	}

	void SetBarwa(Kolor newBarwa)
	{
		this->_barwa = newBarwa;
	}

private:
	Kolor _barwa;
};


int main()
{
	cout << "Pobierz i wyświetl punkt: " << endl;
	auto p = new Punkt(-1, 2, 3);
	p->pobierz();
	p->wyswietl(" ");

	cout << "Pobierz kolorowy punkt: " << endl;
	KolorowyPunkt kp(0, 0, 0, 255, 255, 255);
	kp.pobierz();
	cout << "kolor zmieniony na 100, 100, 100" << endl;
	Kolor k1(100, 100, 100);
	kp.Barwa = k1;
	kp.wyswietl(" ");

	system("pause");
	return 0;
}