#include <iostream>
#include <string>

using namespace std;

// zad 1:
template <typename T>
T NPow(T x, int n) {
	T acc = x;
	for (int i = 1; i < n; ++i) {
		acc *= x;
	}
	return acc;
}

// zad 2:
template <typename T>
void Swap(T &a, T &b) {
	T tmp = a;
	a = b;
	b = tmp;
}

// zad 3:
template <typename T>
class Stos {
public:
	Stos() : rozmiar(0), maxRozmiar(2) {
		this->stos = new T[maxRozmiar];
	}

	~Stos() {
		delete[] this->stos;
	}

	void Wstaw(T elem) {
		if(this->rozmiar == this->maxRozmiar) {
			T* tmp = this->stos;
			this->maxRozmiar *= 2;
			this->stos = new T[this->maxRozmiar];
			for(size_t i = 0; i < this->rozmiar; ++i)
				this->stos[i] = tmp[i];

			delete[] tmp;
		} 
		this->stos[this->rozmiar++] = elem;
	}

	T Zdejmij() {
		if(this->IsEmpty())
			throw ("Stos jest pusty");
		else return this->stos[--this->rozmiar];
	}

	bool IsEmpty() {
		return this->rozmiar == 0;
	}

	void Wyswietl() {
		cout << "stos {";
		for (int i = this->rozmiar - 1; i >= 0; --i)
			cout << this->stos[i] << " ,";
		cout << "}" << endl;
	}


private:
	size_t rozmiar;
	size_t maxRozmiar;
	T* stos;
};

// zad 4:
class Zespolona {
public:
	Zespolona(double re = 0, double im = 0) : im_(im), re_(re) {}

	friend ostream &operator<<(ostream &out, const Zespolona &z) {
		out << '(' << z.re_ << ',' << z.im_ << "i)";
		return out;
	}

	double mod() {
		return sqrt( this->re_ * this->re_ + this->im_ * this->im_ );
	}

	Zespolona operator~() {
		return Zespolona(this->re_, -this->im_);
	}

	friend Zespolona operator*(double x, Zespolona &rhs) {
		return Zespolona( x * rhs.re_, x * rhs.im_);
	}

	friend Zespolona operator*(Zespolona &lhs, double x) {
		return Zespolona( x * lhs.re_, x * lhs.im_);
	}

	Zespolona operator*(Zespolona &rhs) {
		return Zespolona( (this->re_ * rhs.re_) - (this->im_ * rhs.im_),
						  (this->re_ * rhs.im_) + (this->im_ * rhs.re_));
	}

	Zespolona operator+(Zespolona &rhs) {
		return Zespolona(this->re_ + rhs.re_, this->im_ + rhs.im_);
	}

	Zespolona operator-(Zespolona &rhs) {
		return Zespolona(this->re_ - rhs.re_, this->im_ - rhs.im_);
	}

	Zespolona operator/(Zespolona &rhs) {
		return Zespolona( ((this->re_ * rhs.re_) + (this->im_ * rhs.im_)) / ((rhs.re_ * rhs.re_) + (rhs.im_ * rhs.im_)),
						  ((this->im_ * rhs.re_) - (this->re_ * rhs.im_)) / ((rhs.re_ * rhs.re_) + (rhs.im_ * rhs.im_)));
	}

private:
	double im_, re_;
};

int main() {

#pragma region test zad 1
	// test zad 1:
	cout << endl << "----- NPow -----" << endl;
	cout << NPow(2, 4) << endl;
	cout << NPow(2.5, 3) << endl;
#pragma endregion

#pragma region test zad 2
	// test zad 2:
	cout << endl << "----- Swap -----" << endl;
	string s1 = "abc";
	string s2 = "def";
	Swap(s1, s2);
	cout << "s1= " << s1 << endl;
	cout << "s2= " << s2 << endl;

	double d1 = 88.888;
	double d2 = 11.111;
	Swap(d1, d2);
	cout << "d1= " << d1 << endl;
	cout << "d2= " << d2 << endl;
#pragma endregion

#pragma region test zad 3
	// test zad 3:
	cout << endl << "----- Stos -----" << endl;
	Stos<int> si;
	si.Wstaw(1);
	si.Wstaw(2);
	si.Wstaw(3);
	si.Wyswietl();

	cout << si.Zdejmij() << endl;
	cout << si.Zdejmij() << endl;
	cout << si.Zdejmij() << endl;

	Stos<string> ss;
	ss.Wstaw("abc");
	ss.Wstaw("def");
	ss.Wstaw("ghi");

	cout << ss.Zdejmij() << endl;
	cout << ss.Zdejmij() << endl;
	cout << ss.Zdejmij() << endl;
#pragma endregion

#pragma region test zad 4
	// test zad 4
	cout << endl << "----- Liczby zespolone -----" << endl;
	Zespolona z1(3, 4);
	cout << z1 << endl;

	cout << z1.mod() << endl;

	Zespolona z2 = ~z1;
	cout << z2 << endl;

	Zespolona z3 = 2 * z2;
	cout << "z3 = " << z3 << endl;

	Zespolona z4 = z3 * 0.5;
	cout << "z4 = " << z4 << endl;

	cout << z3 + z4 << endl;

	cout << z3 - z4 << endl;

	cout << z3 * z4 << endl;

	cout << z3 / z4 << endl;

#pragma endregion

	system("pause");
	return 0;
}