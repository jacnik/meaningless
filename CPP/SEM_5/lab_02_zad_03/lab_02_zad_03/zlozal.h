/********************
*
* Plik naglowkowy z deklaracjami klas
* przeznaczonymi do badania zlozonosci 
* dowolnych algorytmow
*
*/
#ifndef __cplusplus
# error C++ compiler required
#endif

#ifndef ZLOZAL_H_
#define ZLOZAL_H_

#include <string>

// Klasa bazowa dowolnych algorytmow
// ktorych czas wykonania zalezy od
// jednego parametru 'n' czyli rozmiaru
// wejscia wyznaczajacego zlozonosc algorytmu.
//
class Algorytm {
	int n_; // rozmiar wejscia algorytmu
private: // kopiowanie obiektu algorytmu jest niezdefiniowane
	Algorytm(Algorytm &);
	void operator=(Algorytm &);
public:
	Algorytm(); // tworzy nowy obiekt algorytmu, na poczatku bez wejscia
	virtual ~Algorytm(); // wirtualny destruktor umozliwia poprawne usuwanie obiektow algorytmu

	virtual std::string Nazwa() const = 0; // zwraca nazwe wlasna algorytmu, 
	// metoda musi byc zdefiniowana w klasach pochodnych

	int Rozmiar() const // zwraca biezacy rozmiar wejscia algorytmu
	{ return n_; }

	virtual void Ustaw(int n); // ustawia biezaca strukture danych wejsciowych algorytmu dla podanego rozmiaru wejscia 'n',
	// metoda moze byc redefiniowana (pokryta) w klasach pochodnych

	virtual void Wykonaj() = 0; // glowna procedura wykonawcza obiektu algorytmu, wykonuje algorytm dla biezacej struktury danych wejsciowych,
	// metoda musi byc zdefiniowana w klasach pochodnych
};

// Pojedyncza klasa reprezentujaca urzadzenie
// do uruchomienia procedury wykonujacej podany 
// algorytm i badania zlozonosci algorytmu poprzez
// pomiar czasu jego wykonania.
// 
class Maszyna {
	int n1_, n2_, dn_; // przedzial zmiennosci rozmiaru wejscia dla badanego algorytmu
	int iliter_; // liczba powtorzen wykonywania algorytmu
	int ilczas_; // liczba pomiarow czasu
	double *czas_; // tablica wynikow pomiaru czasu dla kolejnych rozmiarow wejscia
	std::string alg_; // nazwa zbadanego algorytmu
public:
	Maszyna(int n1,int n2,int dn,int m=100); // konstruktor obiektu maszyny 
	~Maszyna(); // destruktor obiektu maszyny

	void Uruchom(Algorytm &Alg); // uruchamia podany algorytm i wykonuje pomiary czasu
	void Wyswietl() const; // wyswietla wyniki pomiarow
};  

#endif /* koniec pliku naglowkowego 'zlozal' */
