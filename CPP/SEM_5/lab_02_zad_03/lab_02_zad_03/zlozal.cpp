#include "zlozal.h" // wlaczenie pliku z deklaracjami klas do badania zlozonosci algorytmow
#include <ctime> // wlaczenie plikow biblioteki standardowej
#include <iostream>
#include <iomanip>
using namespace std;

//
// Definicja skladowych 
// klasy Algorytm
//

Algorytm::Algorytm(): n_(-1)
{}

Algorytm::~Algorytm()
{}

void Algorytm::Ustaw(int n)
{
	n_ = n;
}

//
// Definicja skladowych 
// klasy Maszyna
//

Maszyna::Maszyna(int n1,int n2,int dn,int m): n1_(n1), n2_(n2), dn_(dn), iliter_(m) // konstruktor domyslny, tworzy funktor zmiennej 'x'
{
	ilczas_ = (n2_-n1_)/dn_+1;
	n2_ = n1_+(ilczas_-1)*dn_;
	czas_ = new double[ilczas_];
}

Maszyna::~Maszyna()
{
	delete[] czas_;
}

void Maszyna::Uruchom(Algorytm &Alg)
{
	alg_ = Alg.Nazwa();
	for (int i=0; i<ilczas_; i++) {
		int n = n1_+i*dn_;
		Alg.Ustaw(n);

		clock_t poczatek = clock();
		for (int iter=0; iter<iliter_; iter++) Alg.Wykonaj();
		clock_t koniec = clock();

		czas_[i] = 1000*((double)(koniec-poczatek)/CLOCKS_PER_SEC) / iliter_; 
	}
}

void Maszyna::Wyswietl() const
{
	cout << "Zlozonosc czasowa algorytmu " << alg_ << ":" << endl;
	cout << "   n   | T [ms]" << endl;
	cout << "-------+-------" << endl;
	for (int i=0; i<ilczas_; i++) {
		int n = n1_+i*dn_;
		double T = czas_[i]; 
		cout << setw(6) << n << " | " << setw(5) << setprecision(4) << T << endl;
	}
	cout << endl;
}


