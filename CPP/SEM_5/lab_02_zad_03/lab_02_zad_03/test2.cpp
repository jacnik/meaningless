#include <iostream> // wlaczenie plikow biblioteki standardowej
#include "zlozal.h" // wlaczenie pliku z klasami do badania zlozonosci algorytmow
using namespace std;

//
// Klasy pochodne definiujace konkretne algorytmy
//

class AlgorytmWyszukiwaniaLiniowego : public Algorytm // O(n)
{
private:
	int *dane_;
public:
	AlgorytmWyszukiwaniaLiniowego(): dane_(NULL)
	{}

	~AlgorytmWyszukiwaniaLiniowego()
	{
		if (dane_!=NULL) delete[] dane_;
	}

	string Nazwa() const
	{
		return string("wyszukiwanie liniowe"); 
	}

	void Ustaw(int n)
	{
		Algorytm::Ustaw(n);
		if (this->dane_ != NULL) delete[] this->dane_;
		this->dane_ = new int[n];
		for (int i = 0; i < n; ++i)
		{ 
			this->dane_[i] = i; // data wil be sorted
		}
	}

	void Wykonaj();
};

void AlgorytmWyszukiwaniaLiniowego::Wykonaj() 
{
	int n = Rozmiar();
	int *t = dane_;
	int x = n/2;
	for (int i = 0; i < n; i++) {
		int a = t[i];
		if (x == a) break;
	}
}

class AlgorytmWyszukiwaniaBinarnego : public Algorytm // O(log n) ale tylko dla posortowanych danych
{
private:
	int *dane_;
public:
	AlgorytmWyszukiwaniaBinarnego(): dane_(NULL)
	{}

	~AlgorytmWyszukiwaniaBinarnego()
	{
		if (dane_ != NULL) delete[] dane_;
	}

	string Nazwa() const
	{
		return string("Wyszukiwanie binarne"); 
	}

	void Ustaw(int n)
	{
		Algorytm::Ustaw(n);
		if (this->dane_ != NULL) delete[] this->dane_;
		this->dane_ = new int[n];
		for (int i = 0; i < n; ++i) {
			this->dane_[i] = i; // data will be sorted
		}
	}

	void Wykonaj()
	{
		int n = Rozmiar();
		int *t = dane_;
		int x = n/2;
		int i = 0, j = n-1;

		while (i <= j) {
			int k = (i+j)/2;
			int a = this->dane_[k];
			if (x == a) break;
			if (x < a) j = k-1;
			else i = k+1;
		}
	}
};

//
// Program z przykladam tworzenia i badania p/w algorytmow
//
int main()
{
	Maszyna APU(50000, 1000000, 50000, 1000);
	
	AlgorytmWyszukiwaniaLiniowego L;
	APU.Uruchom(L);
	APU.Wyswietl();
	
	AlgorytmWyszukiwaniaBinarnego B;
	APU.Uruchom(B);
	APU.Wyswietl();

	system("pause");
	return 0;
}
