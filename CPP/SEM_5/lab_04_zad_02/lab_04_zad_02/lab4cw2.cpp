/***************************/
/* Dziedziczenie wirtualne */
/***************************/

#include <string>
#include <iostream>
#include <sstream>
using namespace std;

/* ...uzupelnic kod programu... */
class Kontrolka
{
public:
	Kontrolka(string etykieta = "") : etykieta(etykieta) {}

	virtual string Dialog() = 0;

protected:
	void Naglowek() 
	{ 
		cout << this->etykieta; 
	}

private:
	string etykieta;
};

class ListaWyboru : public Kontrolka
{
public:
	ListaWyboru(string etykieta = "",
				string* opcje = &string(""),
				int licznosc = 0) : Kontrolka(etykieta),
									opcje(opcje),
									licznosc(licznosc){}

	string Dialog() override
	{
		stringstream ss;
		ss << "dost�pne opcje: " << endl;
		for(int i = 0; i < this->licznosc; ++i)
		{
			ss << this->opcje[i] << ", ";
		}
		return ss.str();
	}

protected:
	string Wybor()
	{
		return "ListaWyboru Wybor";
	}

private:
	string* opcje;
	int licznosc;
};

class LiniaEdycyjna : public Kontrolka
{
public:
	LiniaEdycyjna(string etykieta = "",
				  int dlugosc = 0) : Kontrolka(etykieta),
									 dlugosc(dlugosc) {}

	string Dialog() override
	{
		return "LiniaEdycyjna Dialog";
	}

protected:
	string Edycja()
	{
		return "LiniaEdycyjna Edycja";
	}

private:
	int dlugosc;
};

class LiniaZLista : public ListaWyboru, public LiniaEdycyjna
{
public:
	LiniaZLista(string etykieta = "",
		int dlugosc = 0,
		string* opcje = &string(""),
		int licznosc = 0) : LiniaEdycyjna(etykieta, dlugosc),
							ListaWyboru(etykieta, opcje, licznosc)
							{}

	string Dialog()
	{
		return ListaWyboru::Dialog() + " " + LiniaEdycyjna::Dialog();
	}
};

int main()
{
	{
		string opcje[] = { string("opcja A"),
						   string("opcja B"),
						   string("opcja C"), 
						   string("edytuj") };

		ListaWyboru W("test kontrolki z wyborem", opcje, 4);
		LiniaEdycyjna E("test kontrolki z edycja", 10);

		string wynik = W.Dialog();
		cout << "wynik testu wyboru: " << wynik << endl;
		if (wynik == "edytuj") {
			wynik = E.Dialog();
			cout << "wynik testu edycji: " << wynik << endl;
		}
	}

	{
		string opcje[] = { string("opcja A"),
			string("opcja B"),
			string("opcja C") };

		LiniaZLista kontrolka("test kontrolki mieszanej", 10, opcje, 3);

		string wynik = kontrolka.Dialog();
		cout << "wynik testu mieszanego: " << wynik << endl;
	}

	system("pause");
	return 0;
}
