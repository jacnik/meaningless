/*****************************/
/* Dziedziczenie wielobazowe */
/*****************************/

#include <string>
#include <iostream>
#include <sstream>
using namespace std;

/* ...uzupelnic kod programu... */
class Komputer
{
public:
	Komputer(string nazwa = "", 
			 double cena = 0, 
			 string procesor = "", 
			 double pamiec = 0) : nazwa(nazwa), 
								  cena(cena), 
								  procesor(procesor),
								  pamiec(pamiec) {}

	string virtual OpiszKomputer() 
	{
		stringstream ss;
		ss << this->nazwa << "/" << this->procesor << "/"
		   << this->pamiec << "RAM/" << this->cena << "zl";
		return ss.str();
	}

	int ObliczSume(int a, int b)
	{
		return a + b;
	}

private:
	string nazwa;
	double cena;
	string procesor;
	double pamiec;
};

class Wyswietlacz
{
public:
	Wyswietlacz(string nazwa = "",
				double cena = 0,
				string obraz = "",
				double ekran = 0) : nazwa(nazwa),
									cena(cena),
									obraz(obraz),
									ekran(ekran) {}

	string virtual OpiszWyswietlacz() 
	{
		stringstream ss;
		ss << this->nazwa << "/" << this->obraz << "/" 
		   << this->ekran << "cali/" << this->cena << "zl";
		return ss.str();
	}

	void WyswietlLiczbe(int x)
	{
		cout << x << endl;
	}

private:
	string nazwa;
	double cena;
	string obraz;
	double ekran;
};

class Serwer : public Komputer
{
public:
	Serwer(string nazwa = "", 
		   double cena = 0, 
		   string procesor = "", 
		   double pamiec = 0,
		   string magazyn = "",
		   string siec = "") : Komputer(nazwa, cena, 
										procesor, pamiec),
						       magazyn(magazyn), siec(siec) {}

	string OpiszKomputer() override 
	{
		stringstream ss;
		ss << "Serwer: " << magazyn << "/" << siec 
		   << "/" << Komputer::OpiszKomputer();
		return ss.str();
	}

private:
	string magazyn;
	string siec;
};

class Monitor : public Wyswietlacz
{
public:
	Monitor(string nazwa = "",
			double cena = 0,
			string obraz = "",
			double ekran = 0,
			string podstawka = "",
			string kabel = "") : Wyswietlacz(nazwa, cena,
								             obraz, ekran),
								 podstawka(podstawka),
								 kabel(kabel) {}

	string OpiszWyswietlacz() override 
	{
		stringstream ss;
		ss << this->podstawka << "/" << this->kabel << "/"
			<< Wyswietlacz::OpiszWyswietlacz();
		return ss.str();
	}

private:
	string podstawka;
	string kabel;
};

class Tablet : public Komputer, public Wyswietlacz
{
public:
	string OpiszKomputer() override 
	{ 
		stringstream ss;
		ss << "Tablet: " << Komputer::OpiszKomputer(); 
		return ss.str();
	}

	string OpiszWyswietlacz() override 
	{
		stringstream ss;
		ss << "Wyswietlacz: " << Wyswietlacz::OpiszWyswietlacz(); 
		return ss.str();
	}

	string OpiszCalosc()
	{
		stringstream ss;
		ss << this->OpiszKomputer() << " z wyswietlaczem - "
			<< this->OpiszWyswietlacz();
		return ss.str();
	}

	void PokazSume(int a, int b)
	{
		Wyswietlacz::WyswietlLiczbe(Komputer::ObliczSume(a, b));
	}

private:

};

class Pecet : public Komputer
{
public:
	Pecet(string nazwa = "", 
		   double cena = 0, 
		   string procesor = "", 
		   double pamiec = 0,
		   string grafika = "",
		   string wskaznik = "") : Komputer(nazwa, cena, 
										    procesor, pamiec),
								   grafika(grafika), 
								   wskaznik(wskaznik) {}

	string OpiszKomputer() override 
	{ 
		stringstream ss;
		ss << "Pecet: " << this->grafika << "/" << this->wskaznik 
			<< "/" << Komputer::OpiszKomputer()
			<< " -> Monitor: " << this->monitor.OpiszWyswietlacz();
		return ss.str();
	}

	Monitor monitor;
private:
	string grafika;
	string wskaznik;
};

int main()
{         
	Komputer X("Satellite", 3500.0, "Celeron CPU 1.80GHz", 512.0);
	cout << "1) Test komputera X -->" << X.OpiszKomputer() << endl;

	cout << endl;

	Wyswietlacz Y("Belinea", 400.0, "LCD 1024 x 768 True Colors", 21.0);
	cout << "2) Test wyswietlacza Y -->" << Y.OpiszWyswietlacz() << endl;

	cout << endl;
	{
		Serwer S;
		Monitor M;

		cout << "3a) Test serwera -->" << S.OpiszKomputer() << endl;
		cout << endl;
		cout << "3b) Test monitora -->" << M.OpiszWyswietlacz() << endl;

		cout << endl;
		cout << "wynik obliczen sewera wyswietlony na monitorze: ";
		M.WyswietlLiczbe( S.ObliczSume(12, 24) );
	}

	cout << endl;
	{
		Pecet P;

		cout << "4) Test peceta -->" << P.OpiszKomputer() << endl;

		cout << "wynik obliczen peceta wyswietlony na jego monitorze: ";
		P.monitor.WyswietlLiczbe( P.ObliczSume(12, 24) );
	}

	cout << endl;
	{
		Tablet T;

		cout << "5) Test tableta -->" << T.OpiszCalosc() << endl;

		cout << "wynik obliczen wyswietlony na tablecie: ";
		T.WyswietlLiczbe( T.ObliczSume(12, 24) );
		cout << "wynik pokazany na tablecie: ";
		T.PokazSume(12, 24);
	}

	cout << endl;
	system("pause");
	return 0;
}