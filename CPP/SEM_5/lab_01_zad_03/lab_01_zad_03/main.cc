﻿#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

class Podzespol
{
public:
	Podzespol(string nazwa, string producent, double cena) : _nazwa(nazwa), _producent(producent), _cena(cena)
	{
	}

	virtual void wyswietl()
	{
		cout << this->_nazwa << "/" << this->_producent << "/" << this->_cena << " zl";
	}

private:
	string _nazwa;
	string _producent;
	double _cena;
};

class PlytaGlowna : public Podzespol
{
public:
	PlytaGlowna(string producent, double cena) : Podzespol("Plyta Glowna", producent, cena) { }
};

class Zasilacz : public Podzespol
{
public:
	Zasilacz(string producent, double cena) : Podzespol("Zasilacz", producent, cena) { }
};

class KartaSieciowa : public Podzespol
{
public:
	KartaSieciowa(string producent, double cena) : Podzespol("Karta Sieciowa", producent, cena) { }
};

class KartaGrafiki : public Podzespol
{
public:
	KartaGrafiki(string producent, double cena) : Podzespol("Karta Grafiki", producent, cena) { }
};

class DyskTwardy : public Podzespol
{
public:
	DyskTwardy(string producent, double cena) : Podzespol("Dysk Twardy", producent, cena) { }
};

class NagrywarkaDVD : public Podzespol
{
public:
	NagrywarkaDVD(string producent, double cena) : Podzespol("Nagrywarka DVD", producent, cena) { }
};

class Komputer
{
public:
	Komputer(string nazwa) : _nazwa(nazwa), _cena(0), _lpodzespolow(0), _maxPodzespolow(2)
	{
		this->_podzespoly = new Podzespol*[this->_maxPodzespolow];
	}

	~Komputer()
	{
		this->deletePodzespoly();
	}

	// na huj jest ten pierwszy parametr?
	Komputer& Dodaj(int id, Podzespol* podzespol)
	{
		if(this->_lpodzespolow == this->_maxPodzespolow)
		{
			this->_maxPodzespolow *= 2;
			Podzespol** new_podzespoly = new Podzespol*[this->_maxPodzespolow];
			for (unsigned int i = 0; i < this->_lpodzespolow; ++i)
			{
				new_podzespoly[i] = this->_podzespoly[i];
			}
			this->_podzespoly = new_podzespoly;
		}
		this->_podzespoly[this->_lpodzespolow++] = podzespol;
		return *this;
	}

	Komputer& Koniec()
	{
		// co k**rwa ma niby robić?
	}

	string nazwa() { return this->_nazwa; }
	double cena() { return this->_cena; }

	void wyswietl()
	{
		cout << "Komputer: " << this->nazwa() << endl;
		for (unsigned int i = 0; i < this->_lpodzespolow; ++i)
		{
			cout << "	- "; this->_podzespoly[i]->wyswietl();
			cout << endl;
		}
	}

private:
	string _nazwa;
	unsigned int _lpodzespolow;
	Podzespol** _podzespoly;
	double _cena;
	unsigned int _maxPodzespolow;
	
	void deletePodzespoly()
	{
		for (unsigned int i = 0; i < this->_lpodzespolow; ++i)
		{
			delete this->_podzespoly[i];
		}
		delete this->_podzespoly;
	}
};


int main()
{
	Komputer k("hesus");

	auto p1 = new Zasilacz("lenovo", 2.5);
	auto p2 = new KartaSieciowa("intel", 1.25);
	auto p3 = new KartaGrafiki("nvidia", 5.5);
	auto p4 = new PlytaGlowna("intel", 3.3);
	auto p5 = new DyskTwardy("WesterDigital", 3.3);
	auto p6 = new NagrywarkaDVD("samsung", 3.3);

	k.Dodaj(0, p1);	
	k.Dodaj(0, p2);
	k.Dodaj(0, p3);
	k.Dodaj(0, p4);
	k.Dodaj(0, p5);
	k.Dodaj(0, p6);

	k.wyswietl();

	cin.ignore();
	return 0;
}
