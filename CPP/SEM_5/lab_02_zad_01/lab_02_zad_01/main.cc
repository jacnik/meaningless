#include <stdio.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <fstream>

using namespace std;

class Schowek {
protected:
    size_t rozmiar_;

public:
    Schowek() : rozmiar_(0)
    {}
 
    virtual void Zapisz(const void *bufor, size_t rozmiar) = 0;
    virtual void Czytaj(void *bufor, size_t rozmiar) const = 0;
 
    size_t Rozmiar() const
    {
        return rozmiar_;
    }
};

class Blok : public Schowek {

 private:
    void *adres_;
    size_t maks_;

    Blok(Blok &);
    void operator=(Blok &);

 public:
    Blok(): maks_(1024)
    {
        // utworzenie bloku dla poczatkowego maksymalnego rozmiaru i zapamietanie jego adresu
        this->adres_ = malloc(maks_);
    }

    ~Blok()
    {
        // usuniecie bloku
        free(adres_);
    }

    void Zapisz(const void *buf, size_t roz)
    {
        // realokacja bloku gdy rozmiar bufora przekroczy maksymalny rozmiar bloku
        // skopiowanie danych z bufora do bloku
        // zapamietanie rozmiaru bufora jako biezacego rozmiaru schowka
        if(roz > this->maks_) {
            this->maks_ = roz * 2;
            //realloc(adres_, this->maks_);
            //void* newAdres = malloc(maks_);
            //memcpy(adres_, buf, roz);
            free(adres_);
            this->adres_ = malloc(maks_);
        }
       
        memcpy(adres_, buf, roz);
        this->rozmiar_ = roz;   
    }

    void Czytaj(void *buf, size_t roz) const
    {
        // sprawdzenie czy wystapil przypadek przekroczenia rozmiaru schowka pzez rozmiar bufora
        // skopiowanie danych z bloku do bufora
        if (this->rozmiar_ <= roz) {
            memcpy(buf, adres_, this->rozmiar_);
        }
    }
};

class Plik : public Schowek {
 private:
    static int licznik_;
    char nazwa_[256];

    Plik(Plik &);
    void operator=(Plik &);

public:
    Plik()
    {
        licznik_++;
        sprintf(nazwa_, "plikdanych%d.bin", licznik_);
    }

    ~Plik()
    {
        licznik_--;
        // usuniecie pliku z dysku poprzez podanie jego nazwy
        remove(this->nazwa_);
    }

    void Zapisz(const void *buf, size_t roz)
    {
        // otworzenie pliku o podanej nazwie
        // zapisanie danych z bufora do pliku
        // zamkniecie pliku
        // zapamietanie rozmiaru bufora jako biezacego rozmiaru schowka

        FILE* pfile = fopen(this->nazwa_, "wb");
        if(pfile != NULL)
        {
            
			fwrite(buf, sizeof(buf), roz/sizeof(buf), pfile);
            fclose(pfile);
            this->rozmiar_ = roz;
        }
    }

    void Czytaj(void *buf, size_t roz) const
    {
        // sprawdzenie czy wystapil przypadek przekroczenia rozmiaru schowka pzez rozmiar bufora
        // odczytanie danych z pliku do bufora
        // zamkniecie pliku
        if (this->rozmiar_ <= roz) {
            FILE* pfile = fopen(this->nazwa_, "rb");
			fread(buf, sizeof(buf), roz/sizeof(buf),  pfile);
            fclose(pfile);
        }
    }
};

int Plik::licznik_ = 0;

double TestSchowka(Schowek &X)
{
	const int nmin = 10;
	const int nmax = 10*1024;
	float tab[nmax] = {0.0};

	clock_t poczatek = clock();
 
	for (int n = nmin; n <= nmax; n++) {
		if (n == 2906)
		{
			cout << "";
		}
		size_t roz = n*sizeof(float);
		X.Zapisz(tab, roz);
		X.Czytaj(tab, roz);
	}
 
	clock_t koniec = clock();
 
	return (double)(koniec-poczatek)/CLOCKS_PER_SEC;
}

int main()
{
 cout << endl;
 {
    const int n = 5;
    const int tab1[n] = {11,21,31,41,51};
    const int tab2[n] = {12,22,32,42,52};
    const int tab3[n] = {13,23,33,43,53};

    Blok A1,A2,A3;
    A1.Zapisz(tab1, n*sizeof(int));
    A2.Zapisz(tab2, n*sizeof(int));
    A3.Zapisz(tab3, n*sizeof(int));

    int tab[n]; 
    A1.Czytaj(tab, n*sizeof(int));
    cout << "Odczytane dane z bloku pierwszego:" << endl;
    for (int i=0; i<n; i++)
        cout << i << ") = " << tab[i] << endl;

    A2.Czytaj(tab, n*sizeof(int));
    cout << "Odczytane dane z bloku drugiego:" << endl;
    for (int i=0; i<n; i++)
        cout << i << ") = " << tab[i] << endl;

    A3.Czytaj(tab, n*sizeof(int));
    cout << "Odczytane dane z bloku trzeciego:" << endl;
    for (int i=0; i<n; i++)
        cout << i << ") = " << tab[i] << endl;
 }

 cout << endl;
 {
	const int n = 5;
	const int tab1[n] = {11,21,31,41,51};
	const int tab2[n] = {12,22,32,42,52};
	const int tab3[n] = {13,23,33,43,53};

	Plik A1,A2,A3;
	A1.Zapisz(tab1, n*sizeof(int));
	A2.Zapisz(tab2, n*sizeof(int));
	A3.Zapisz(tab3, n*sizeof(int));

	int tab[n]; 
	A1.Czytaj(tab, n*sizeof(int));
	cout << "Odczytane dane z pliku pierwszego:" << endl;
	for (int i=0; i<n; i++)
		cout << i << ") = " << tab[i] << endl;

	A2.Czytaj(tab, n*sizeof(int));
	cout << "Odczytane dane z pliku drugiego:" << endl;
	for (int i=0; i<n; i++)
		cout << i << ") = " << tab[i] << endl;

	A3.Czytaj(tab, n*sizeof(int));
	cout << "Odczytane dane z pliku trzeciego:" << endl;
	for (int i=0; i<n; i++)
		cout << i << ") = " << tab[i] << endl;
 }

	cout << endl;
	{
		Blok blok;
		Plik plik;
		double bczas, pczas;
		cout << "test przetwarzania bloku danych... "; cin.ignore(1);
		bczas = TestSchowka(blok);
		cout << "czas testu dla bloku danych: " << bczas << " s" << endl;
		cout << "test przetwarzania pliku danych... "; cin.ignore(1);
		pczas = TestSchowka(plik);
		cout << "czas testu dla pliku danych: " << pczas << " s" << endl;
	}
 
	cout << endl;
	system("pause");
	return 0;
}