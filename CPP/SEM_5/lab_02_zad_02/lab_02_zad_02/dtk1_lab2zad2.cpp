﻿#include <iostream>
#include <iomanip>

using namespace std;

/* !! Tu wpisac kod klas !!
 */
class Figura
{
public:
	Figura(int x) : rozmiar_(x > 0 ? x : -x) {};

	virtual void rysuj() = 0;
	virtual void powieksz(int dx) 
	{ 		
		if ( dx >= 0 ) {
			this->rozmiar_ *= dx;
		} else {
			this->rozmiar_ /= -dx;
		}
	}

	int daj_rozmiar() { return this->rozmiar_; };

private:
	int rozmiar_;
};

class Prostokat : public Figura
{
public:
	Prostokat(int wys, int szer) : Figura(wys*szer), wys_(wys > 0 ? wys : -wys), szer_(szer > 0 ? szer : -szer) 
	{ }

	void powieksz(int dx) override
	{
		if ( dx >= 0 ) {
			this->wys_ *= dx;
			this->szer_ *= dx;
			Figura::powieksz(dx*dx);
		} else {
			this->wys_ /= -dx;
			this->szer_ /= -dx;
			Figura::powieksz( -(dx*dx) );
		}
		
	}

	void rysuj() override
	{
		//cout << setfill('*') << setw(4);
		for(int i = 0; i < this->wys_; ++i) {
			cout << setfill('*') << setw(this->szer_) << "" << endl;;
		}
	}

private:
	int wys_;
	int szer_;
};

// najwyraźniej rownoramienny bo ma tylko gługość jednej podstawy
class Trojkat : public Figura
{
public:
	Trojkat(int podst) : Figura(podst), podst_(podst) { }

	void powieksz(int dx) override
	{
		if ( dx >= 0 ) {
			this->podst_ *= dx;
		} else {
			this->podst_ /= -dx;
		}
		Figura::powieksz(dx);
	}

    void rysuj() override
	{
		for (int i = 0; i < this->podst_; ++i) {
            for (int j = 0; j <= i; ++j)
				cout << "*";
			cout << endl;
		}
	}

private:
	int podst_;
};

class Kolo : public Figura
{
public:
	Kolo(int prom): Figura(prom), prom_(prom) { }

	void powieksz(int dx) override
	{
		if ( dx >= 0 ) {
			this->prom_ *= dx;
		} else {
			this->prom_ /= -dx;
		}
		Figura::powieksz(dx);
	}

	void rysuj() override
	{
		for (int i = 0; i <= 2*this->prom_; i++)
		{
			for (int j = 0; j <= 2*this->prom_; j++)
			{
				double distance_to_centre = sqrt(double(i - this->prom_)*(i - this->prom_) + (j - this->prom_)*(j - this->prom_));
				if (distance_to_centre > this->prom_ - 0.5 && distance_to_centre < this->prom_ + 0.5)
					 cout << "*";
				else
					 cout << " ";
			}
			cout << endl;
		}
	}

private:
	int prom_;
};

void rysuj_obrazek(Figura *fig)
{
	int linie;
	cout << "*******************" << endl;
	linie = 0;
	while (linie < 20) {
		fig->rysuj();
		linie += fig->daj_rozmiar();
		cout << endl;
		fig->powieksz(2);
	}

	linie = 0;
	while (linie < 20) {
		fig->rysuj();
		linie += fig->daj_rozmiar();
		cout << endl;
		fig->powieksz(-2);
	}
	cout << "*******************" << endl;
}

void main()
{
	Prostokat f1(3, 6);
	rysuj_obrazek(&f1);

	cin.ignore(2);

	Trojkat f2(5);
	rysuj_obrazek(&f2);

	cin.ignore(2);

	Kolo f3(4);
	rysuj_obrazek(&f3);

	cin.ignore(2);
}
