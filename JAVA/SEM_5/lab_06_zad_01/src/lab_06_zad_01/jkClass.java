package lab_06_zad_01;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class jkClass extends JFrame implements ActionListener {

	/**
	 * JK
	 */
	private static final long serialVersionUID = 1L;
	private JTextField szer_txt;
	private JTextField dlug_txt;
	private JTextField cena_txt;
	private JLabel result_lbl;
	private JComboBox<String> grunt_combo;
	private JCheckBox podatek_cb;
	
	public static void main(String[] args) {

		jkClass mojaKl = new jkClass();
		mojaKl.create();
	}

	private void create() {
		this.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;

		// szerokosc
		JLabel szer_lbl = new JLabel("Szeroko��");
		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 0;
		this.add(szer_lbl, c);

		this.szer_txt = new JTextField(20);
		c.gridx = 0;
		c.gridy = 1;
		this.szer_txt.setInputVerifier(new MyInputVerifier());
		this.add(szer_txt, c);

		// dlugosc
		JLabel dlug_lbl = new JLabel("D�ugo��");
		c.gridx = 1;
		c.gridy = 0;
		this.add(dlug_lbl, c);

		this.dlug_txt = new JTextField(20);
		c.gridx = 1;
		c.gridy = 1;
		this.dlug_txt.setInputVerifier(new MyInputVerifier());
		this.add(dlug_txt, c);

		// cena
		JLabel cena_lbl = new JLabel("Cena");
		c.gridx = 2;
		c.gridy = 0;
		this.add(cena_lbl, c);

		this.cena_txt = new JTextField(20);
		c.gridx = 2;
		c.gridy = 1;
		this.cena_txt.setInputVerifier(new MyInputVerifier());
		this.add(cena_txt, c);

		// Button oblicz
		JButton oblicz_btn = new JButton("Oblicz");
		c.weightx = 0.5;
		c.gridx = 2;
		c.gridy = 2;
		c.fill = GridBagConstraints.WEST;
		c.anchor = GridBagConstraints.LINE_START;
		oblicz_btn.setActionCommand("Oblicz_Clicked");
		oblicz_btn.addActionListener(this);
		this.add(oblicz_btn, c);

		// resultat lbl
		this.result_lbl = new JLabel();
		c.gridx = 2;
		c.gridy = 2;
		c.fill = GridBagConstraints.RELATIVE;
		c.anchor = GridBagConstraints.LINE_END;
		// c.anchor = GridBagConstraints.RELATIVE;
		this.add(result_lbl, c);

		// grunt / budynek mieszkalny comboBox
		this.grunt_combo = new JComboBox<String>();
		this.grunt_combo.addItem("Grunt");
		this.grunt_combo.addItem("BudynekMieszkalny");
		c.gridx = 1;
		c.gridy = 2;
		c.anchor = GridBagConstraints.LINE_START;
		this.add(this.grunt_combo, c);

		// podatek CheckBox
		this.podatek_cb = new JCheckBox("Dolicz podatek");
		c.gridx = 0;
		c.gridy = 2;
		this.add(this.podatek_cb, c);
		
		// wyczysc pola btn
		JButton czysc_btn = new JButton("Wyczy��");
		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 3;
		c.fill = GridBagConstraints.WEST;
		c.anchor = GridBagConstraints.LINE_START;
		czysc_btn.setActionCommand("Wyczysc_Clicked");
		czysc_btn.addActionListener(this);
		this.add(czysc_btn, c);
		
		this.setMinimumSize(new Dimension(500, 300));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getActionCommand().equals("Oblicz_Clicked")) {
			double wynik = 0;
			try {
				wynik = new Double(this.dlug_txt.getText())
						* new Double(this.szer_txt.getText())
						* new Double(this.cena_txt.getText());
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(this,
						"Podaj wszystkie warto�ci",
						"Puste pole", JOptionPane.ERROR_MESSAGE);
			}
			wynik *= this.grunt_combo.getSelectedIndex() == 0 ? 0.8 : 1.5;
			wynik *= this.podatek_cb.isSelected() ? 1.23 : 1;
			this.result_lbl.setText(Double.toString(wynik).concat(" z�"));
		} else if (arg0.getActionCommand().equals("Wyczysc_Clicked")) {
			this.szer_txt.setText("");
			this.dlug_txt.setText("");
			this.cena_txt.setText("");
			this.result_lbl.setText("");
			this.grunt_combo.setSelectedIndex(0);
			this.podatek_cb.setSelected(false);
		}
	}
}

class MyInputVerifier extends InputVerifier {
	@Override
	public boolean verify(JComponent input) {
		String text = ((JTextField) input).getText();
		if (text.isEmpty())
			return true;
		try {
			double value = new Double(text);
			if (value <= 0) {
				alert(input);
				return false;
			}
			return true;
		} catch (NumberFormatException e) {
			this.alert(input);
			return false;
		}
	}

	private void alert(JComponent text) {
		JOptionPane.showMessageDialog(text,
				"Podana warto�� jest nieprawid�owa", "Z�a warto��",
				JOptionPane.ERROR_MESSAGE);
	}
}